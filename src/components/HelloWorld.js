import React from 'react'
const HelloWorld = () => {
  return (
    <div className='hello-world'>
      <h1>Hello React</h1>
      <p>React project starts here...</p>
    </div>
  )
}
export default HelloWorld